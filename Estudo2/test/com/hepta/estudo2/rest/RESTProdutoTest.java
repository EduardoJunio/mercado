package com.hepta.estudo2.rest;

import java.util.List;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.estudo2.dto.ProdutoMercadoDTO;
import com.hepta.estudo2.entity.Produto;
import com.hepta.estudo2.entity.Usuario;
import com.hepta.estudo2.persistence.UsuarioDAO;
import com.mysql.cj.log.Log;

public class RESTProdutoTest {

	Produto produto = new Produto();
	RESTProduto rest = new RESTProduto();
	String nome = "nome";
	String categoria = "categoria";
	String marca = "marca";
	float valor = 123;
	int idUsuario = 1;
	int idMercado = 1;
	

	@Test
	void testCadastro() throws Exception {
		produto.setCategoria(categoria);
		produto.setNome(nome);
		produto.setMarca(categoria);
		produto.setValor(valor);
		produto.setIdUsuario(idUsuario);
		produto.setMercadoid(idMercado);
		Response response = rest.insert(produto);
		assert(response.getStatus() == 200);
			
	}
	
	@Test
	void testTodosProdutos() throws Exception {
		List<ProdutoMercadoDTO> produtos = rest.todosProdutos(Integer.toString(idUsuario));
		GenericEntity<List<ProdutoMercadoDTO>> entity = new GenericEntity<List<ProdutoMercadoDTO>>(produtos) {
		};
		
	}
	
	
	
	


}

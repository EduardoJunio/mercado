package com.hepta.estudo2.rest;

import static org.junit.jupiter.api.Assertions.*;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.estudo2.entity.Usuario;
import com.hepta.estudo2.persistence.UsuarioDAO;

class RESTUsuarioTest {
	
	RESTUsuario rest = new RESTUsuario();
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
	}

	@Test
	void testCadastro() throws Exception {
		// DADO
		Usuario user = new Usuario();
		user.setEmail("testehepta@gmail.com");
		user.setSenha("12345");
		user.setNome("teste");
		Response response = rest.insert(user);
		assert(response.getStatus() == 200);
			
	}
	
	@Test
	void testLogin() throws Exception {
		// DADO
		Usuario user = new Usuario();
		UsuarioDAO dao = new UsuarioDAO();
		user.setEmail("testehepta@gmail.com");
		user.setSenha("12345");
		assert(dao.getLogin(user) != null);
		
	
	}

}
